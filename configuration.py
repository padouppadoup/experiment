#-*-coding:utf-8-*-
# Experiment framework
# By MW, Sept 2016
# GPLv3+

# ==== CONFIGURATION FILE
# This file contains the main parameters to run the 'experiment command'
# ====

# ==== Platform
## Use a SLURM scheduler
use_slurm = False 

# ==== OUTPUT CONFIG.
## Location of the output folders
default_output_path = '/home/maxime/code/experiment/outputs/'

## Add date timestamp to the output folders
add_timestamp = True

# ==== MODULES
modules = {'cs': {'bin': '/home/maxime/code/experiment/bin/cs/',
                  'path': 'cs_reconstructions',
                  'subpath': True}}
