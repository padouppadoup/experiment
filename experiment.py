#-*-coding:utf-8-*-
# Experiment framework
# By MW, Sept 2016
# GPLv3+

# ==== Imports
import sys, getopt, os, datetime, shutil, logging, time
sys.path.append('.') # Add the path of the configuration file
import configuration as config

# ==== Variables
version = '0.1'

# ==== Display help
if len(sys.argv) == 1: # Check that we have an input
    raise getopt.GetoptError("No parameters input. For input instructions type 'experiment help'")

if sys.argv[1].lower() == 'help':
    print "Here is a little help"
    sys.exit(0)

elif sys.argv[1].lower() == 'version':
    print "Using version {}".format(version)

# ==== Create the folder
elif sys.argv[1].lower() == 'new' and len(sys.argv) == 2:
    raise getopt.GetoptError("Cannot call 'new' without argument")

elif sys.argv[1].lower() == 'new':
    if len(sys.argv) == 3: ## use the standard output folder
        bp = config.default_output_path
    elif len(sys.argv) == 4: ## use a custom standard output determined by the prefix
        method = sys.argv[2]
        if method in config.modules:
            method_params = config.modules[method]
            if method_params['subpath']:
                bp = os.path.join(config.default_output_path, method_params['path'])
            else:
                bp = method_params['path']
        else:
            raise IOError("'method' {} is not registered/unknown".format(method))
    else:
        raise getopt.GetoptError("Too many parameters for the 'new' option")
    
    # Generate filename
    prefix = ""
    if config.add_timestamp:
        dt = datetime.datetime.now()
        prefix += dt.strftime("%Y%m%d")
    fn = prefix + "_" + sys.argv[2]
    fp = os.path.join(bp, fn) # Generate full path

    # Sanity checks on paths
    print "Creating a new experiment in folder:{}".format(fp)
    if not os.path.isdir(bp):
        raise IOError("Output base folder does not exists: {}".format(bp))
    if os.path.isdir(fp):
        raise IOError("Output experiment folder already exists: {}".format(fp))

    # Get description
    print "Enter a short description of the experiment:"
    descr = raw_input("Description: ")    
    
    # Create folder
    os.makedirs(fp)
    shutil.copytree(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config/'),
                    os.path.join(fp, 'config'))
    for i in ['scripts', 'data', 'results']:
        os.makedirs(os.path.join(fp, i))
    with open(os.path.join(fp, "DESCRIPTION"), 'w') as f:
        f.write(descr+"\n")
        
    print "-- Folder created successfully"

    
# ==== Actually process the stuff
elif sys.argv[1].lower() == 'run' and len(sys.argv) == 2:
    raise getopt.GetoptError("Cannot call 'run' without argument")
elif sys.argv[1].lower() == 'run' and len(sys.argv) < 6:
    raise getopt.GetoptError("Cannot call 'run {}' without argument".format(sys.argv[2]))


elif sys.argv[1].lower() == "run":
    expname = sys.argv[4]
    configfile = sys.argv[5]
    
    # Get the path of the experiment
    bp = config.default_output_path
    method = sys.argv[2]
    if method in config.modules:
        method_params = config.modules[method]
        if method_params['subpath']:
            bp = os.path.join(config.default_output_path, method_params['path'])
        else:
            bp = method_params['path']        
    else:
        raise IOError("'method' {} is not registered/unknown".format(method))
    folder = sys.argv[3]
    fp = os.path.join(bp, folder)
    if not os.path.isdir(fp):
        raise IOError("Cannot find the input folder: {}".format(fp))

    # Checking the input files
    op = os.path.join(fp, 'results', expname)
    if os.path.isdir(op):
        raise IOError("Output experiment folder already exists: {}".format(op))

    cp = os.path.join(fp, 'config', configfile)
    if not os.path.isfile(cp):
        raise IOError("Cannot find config file: {}".format(cp))

    # Create output dirs
    os.makedirs(op)
    shutil.copyfile(cp, os.path.join(op, "config.py"))
    cp_origin = cp
    cp = os.path.join(op, "config.py")
    lp = os.path.join(op, "experiment.log")

    # Start the logging system
    FORMAT = '%(asctime)-15s %(scope)s %(message)s'
    logging.basicConfig(filename=lp, level=logging.DEBUG, format=FORMAT) # 
    de = {'scope': expname}
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.info('Experiment started: "{}"'.format(method), extra=de)
    logger.info('Output folder: "{}"'.format(op), extra=de)
    logger.info("Configuration path: {}".format(cp), extra=de)
    logger.info("Logging path: {}".format(lp), extra=de)
    logger.info("---------------------------------------", extra=de)
    logger.info("", extra=de)

    # Load the code
    sys.path.append(method_params['bin'])
    import expmodule
    logger.info("Running pipeline version: {}".format(expmodule.get_version()), extra=de)

    # ===== Running
    print "Starting logging system: {}".format(lp)
    print "Working on {}".format(fp)
    print "Experiment name is {}".format(expname)
    print "Running experiment! uses config file: {}".format(cp_origin)

    tic = time.time()
    expmodule.run(main_folder=fp,
                  output_folder=op,
                  config_file=cp,
                  logger=(logger, de),
                  config_general=config)
    toc = time.time()
    print "Experiment finished in {}s".format(toc-tic)
    logger.info("Experiment finished in {}s".format(toc-tic), extra=de)

# ==== Command has not be found
else:
    raise getopt.GetoptError("Unknown command '{}'. For input instructions type 'experiment help'".format(sys.argv[1]))
    
