## Initialize the reconstruction. Here should be the main variables
## Maxime W., Jul 2016, GPLv3+
## Called from: python ../8_cluster/tars/03_reconstruct_3d/reconstruction_init.py $mode3d $inpdir $outdir $restrictlist $dict_samp $dict_atom

## ==== Imports
import h5py, os, datetime, sys
from libtiff import TIFF
import numpy as np
td=datetime.datetime.now().strftime('%Y%m%d')

## ==== Variables (They will be imported by the other scripts)
def init(config, logger):
    # ==== Deparse inputs
    de = {'scope': 'reconstruction.py'}
    logger.debug("Entering 'reconstruction.py'", extra=de)
    dict_file = config.dict_file
    basis_file = config.basis_file
    tmp_folder = os.path.join(config.out_folder, 'tmp')
    mode3d = config.mode3d
    dict_samp = config.dict_samp
    dict_atom = config.dict_atom
    inp_folder = os.path.join(config.main_folder, 'data', config.inpdir)
    out_folder = config.out_folder
    restrictfile = os.path.join(config.main_folder, config.restrictlist)

    ## ==== Check for input images
    l_exi = [i for i in os.listdir(os.path.join(inp_folder)) if i.endswith('.tif') and not i.startswith('ref_') and not i.endswith(basis_file.replace('.csv','.tif'))]
    if len(l_exi) == 0:
        raise IOError('There is no .tif image in the input folder {}'.format(inp_folder))

    ## ==== Check for restriction list
    if os.path.isfile(restrictfile):
        print('Opening a list of selected files: {}'.format(restrictfile))
        with open(restrictfile, 'r') as f:
            l_paths = f.readlines()
            l_paths = [i[:-1] for i in l_paths]
            if False in [i in l_exi for i in l_paths]:
                raise IOError('Some files in the restrictlist do not exist in the folder')
            l_exi = l_paths

    # ==== Get the dimensions of the 
    im=TIFF.open(os.path.join(inp_folder,l_exi[0]))
    for i in im.iter_images():
        nx = i.shape[0]
        break

    # ==== Check for the input basis
    b=np.genfromtxt(os.path.join(inp_folder, basis_file))

    # ==== Checks for the HDF file
    fp = os.path.join(inp_folder, dict_file)
    if not os.path.isfile(fp):
        raise IOError('file {} not found. ABORTING'.format(fp))
    if not os.access(fp, os.R_OK):
        raise IOError('file {} not readable or not found. ABORTING'.format(fp))
    with h5py.File(fp, 'r') as hd:
        if not ('dictionary') in hd.keys():
            raise KeyError("The input file doesn't seem to contain the required structure (dictionary and reconstructions dataset). ABORTING")
        dict_found = False
        for d in hd['dictionary'].values():
            if d.attrs['samp']==dict_samp and d.attrs['atom']==dict_atom:
                dict_found = True
                break
            if not dict_found:
                raise KeyError('The required dictionary has not been found in the .hdf5: looking for {} samples and {} atoms'.format(dic_samp, dict_atom))

    ## ==== Initialize for 2D or 3D mode.
    l_exi = [i for i in l_exi if not i in os.listdir(out_folder)] # Exclude the existing reconstructions
    ninp=0
    if mode3d:
        c_rec = zip(l_exi, ['na']*len(l_exi), [os.path.join(out_folder, i) for i in l_exi])
        ninp += len([i for i in os.listdir(out_folder) if i.endswith('.tif')])
    else:
        if not os.path.isdir(tmp_folder):
            os.mkdir(tmp_folder)
            l_exi_exi = []
        else:
            l_exi_part = os.listdir(tmp_folder) # To be substracted from the list
            l_exi_frame = [i.split('.')[-3] for i in l_exi_part]
            l_exi_pos = [int(i.split('.')[-2]) for i in l_exi_part]
            l_exi_exi = zip(l_exi_frame, l_exi_pos)
        
        c_rec = []
        for i in l_exi:
            c_rec += zip([i]*nx, range(nx))
        c_rec = [(i[0], i[1], os.path.join(tmp_folder, "{}.{}.csv".format(i[0], i[1]))) for i in c_rec]
        c_rec = [i for i in c_rec if i not in l_exi_exi] ## to debug
        ninp += len([i for i in os.listdir(tmp_folder) if i.endswith('.csv')])

    ## ==== Compute the full set of parameters to be written
    c_pro = zip([os.path.join(inp_folder, i) for i in zip(*c_rec)[0]], 
                zip(*c_rec)[1], 
                [dict_samp]*len(c_rec), 
                [dict_atom]*len(c_rec), 
                [fp]*len(c_rec),
                [os.path.join(inp_folder, basis_file)]*len(c_rec),
                zip(*c_rec)[2],
    )
    c_pro = [i for i in c_pro if i[6] not in [os.path.join(tmp_folder,j) for j in os.listdir(tmp_folder)]]
    
    with open(os.path.join(out_folder, 'to_process.txt'), 'w') as o:
        for i in c_pro:
            o.write('{} {} {} {} {} {} {}\n'.format(*i))

    print('  Number of already existing files: {}'.format(ninp))
    print('  Number of reconstructions to perform: {}'.format(len(c_pro)))
    print "\n-- Computing the list of reconstructions to be performed"
    print "  (be careful, this does not check for ongoing reconstructions"
    print "  thus, those might be performed several times if great care"
    print "  is not taken)."
    print "  Instructions for reconstructions have been saved in {}.".format(os.path.join(out_folder, 'to_process.txt'))

    return len(c_pro)

            
## ==== Run in standalone mode
if __name__ == '__main__':
    (none, mode3d, inp_folder, out_folder, restrictfile, dict_samp, dict_atom) = sys.argv
    print "I know that this part of the code is broken. Sorry"
    init(mode3d, inp_folder, out_folder, restrictfile, dict_samp, dict_atom)
    
    print('-- Initializing')
    print('  Input folder: {}'.format(inp_folder))
    print('  Output folder: {}'.format(out_folder))
    print('  Files to process in the file: {}'.format(restrictfile))
    print('  Selected mode: {}'.format(['2d', '3d'][mode3d]))
    print('  Number of already existing files: {}'.format(ninp))
    print('  Number of reconstructions to perform: {}'.format(len(c_pro)))
    print "\n-- Computing the list of reconstructions to be performed"
    print "  (be careful, this does not check for ongoing reconstructions"
    print "  thus, those might be performed several times if great care"
    print "  is not taken)."
    print
    with open(os.path.join(out_folder, 'to_process.txt'), 'w') as o:
        for i in c_pro:
            o.write('{} {} {} {} {} {} {}\n'.format(*i))
