## Maxime W., Jul 2016, GPLv3+
## This is actually the worker

## ==== Importations
import sys, logging, time, h5py, multiprocessing, os
import numpy as np
from libtiff import TIFF
sys.path.append('../3_code/')
import cstools
chunksize = 8
ncores=32
method=1

print('  warning, the path of the log file is hardcoded')
## ==== Initialize parameters
(none, frame, xpos, samp, atom, dict_path, basis_path, out_path, logfile, pid2) = sys.argv ## Deparse input
samp = int(samp);atom=int(atom);
if xpos == 'na': # 3D mode
    pass
else:
    xpos=int(xpos)
    #out_name = 'rec_{}_{}_{}_{}.pickle'.format(samp, atom, zpos, fn)
    #out_fn = os.path.join(out_folder, out_name)

## ==== logging
FORMAT = '%(asctime)-15s %(scope)s %(user)-8s %(message)s'
#logging.basicConfig(filename=log_file.format(pid1, pid2), level=logging.DEBUG, format=FORMAT) # 
logging.basicConfig(filename=logfile, level=logging.DEBUG, format=FORMAT) # 
de = {'scope': "{}.{}".format(os.path.basename(frame), xpos), 'user': pid2}
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.info('-------------------------------------', extra=de)
logger.info('Starting the 3d reconstruction code', extra=de)
logger.info('Working on stack {}, x-position is {}'.format(frame, xpos), extra=de)
logger.info('Dictionary parameters are {} atoms and {} samples'.format(atom, samp), extra=de)
logger.info('Parallelism info: using {} cores and a chunksize of {}'.format(ncores, chunksize), extra=de)
logger.info('Now loading the input data & parameters', extra=de)

## ==== Load everything
im=TIFF.open(frame) # Load the images
image = []
for i in im.iter_images():
    image.append(i)
in_image = np.zeros((image[0].shape[0], image[0].shape[1], len(image)))
for (i,j) in enumerate(image):
    in_image[:,:,i]=j

with h5py.File(dict_path, 'r') as hd: # Load the dictionary
    d=[ii for ii in hd['dictionary'].values() if ii.attrs['samp']==samp and ii.attrs['atom']==atom][0]
    name = d.name
    d = np.asarray(d)
    
b=np.genfromtxt(basis_path)
bb = b.dot(d) ## The virtual dictionary      

print('dictionary shape: {}'.format(d.shape))
print('basis shape: {}'.format(b.shape))
print('image shape: {}'.format(in_image.shape))

logger.info('Parameters have been loaded', extra=de)
print('  Parameters have been loaded. Running the reconstruction')

## ==== Actually perform the reconstruction
tic = time.time()
if xpos=='na':
    logger.info('Now performing the reconstruction in 3D', extra=de)
    r = cstools.reconstruct_parallel3d(in_image,bb, ncores=multiprocessing.cpu_count())
    r=[np.dot(d, r[i,:,:].T) for i in range(r.shape[0])]
    out = np.zeros((r[0].shape[0], r[0].shape[1], len(r)))
    for (i,j) in enumerate(r):
        out[i,:,:]=j
else:
    logger.info('Now performing the reconstruction in 2D (one 2D slice)', extra=de)
    if method==0:
        j = in_image[xpos,:,:]
        r = cstools.reconstruct_parallel2d(j, bb, ncores=multiprocessing.cpu_count()) # old method
    elif method==1:
        j = in_image[xpos,:,:]
        print(j.shape, bb.shape)
        def algo(a,b):
            return cstools.reconstruct_1Dspiral(a, b, noisetype='poisson', maxiter=2000)
            return cstools.reconstruct_1Dspiral(a, b)
        r = cstools.reconstruct_2d(j, bb, algo=algo, chunksize=chunksize, ncores=ncores)
    r = np.dot(d, r.T)
toc = time.time()
logger.info('Reconstruction is over and took {}s'.format(toc-tic), extra=de)

# ==== Save everythin'
logger.info('Now saving', extra=de)
if xpos == 'na':
    tif = TIFF.open(out_path, mode='w')
    ost = r.swapaxes(2, 0).swapaxes(1,2) ## Right dynamic range and axis order
    tif.write_image(np.array(ost, dtype=np.uint16))
    tif.close()
else:
    np.savetxt(out_path, r)
logger.info('Reconstruction has been saved at {}'.format(out_path), extra=de)
print('  All done in {}s!'.format(toc-tic))
