#-*-coding:utf-8-*-
# Experiment framework
# By MW, Sept 2016
# GPLv3+
#
# Glue code for compressed sensing stuff. There shouldn't be too much stuff in this script
#+just glue code

# ==== Imports
import sys, os, subprocess
import reconstruction

# ==== Variables
version = '0.1'

def get_version():
    return version

def run(main_folder, output_folder, config_file, logger, config_general):
    """This is the main function"""
    # ==== Deparse params
    de = logger[1]
    de['scope']='expmodule'
    logger = logger[0]
    sys.path.append(os.path.dirname(config_file))
    print config_file
    import config
    config.out_folder = output_folder
    config.main_folder = main_folder
    
    # ==== Printing fancy stuff
    print
    print "==== Compressed sensing pipeline ===="
    print "This script is designed to perform reconstruction of selected"
    print "frames using various dictionaries. Make sure that you have run"
    print "preprocessing stuff before running this script. Also, note that"
    print "the script is designed to work on Institut Pasteur cluster's"
    print "'tars'. Modifications could be needed in order to make it work"
    print "on other architectures"
    print

    if config.useslurm: # Print disclaimer for SLURM users
        print "==== DISCLAIMER ===="
        print "Have you installed the following packages?: virtualenv, numpy, scipy libtiff, joblib, pycsalgos, h5py, pySPIRALTAP"
        print "Have you run the following commands to load the packages?"
        print "$ module load Python/2.7.11"
        print "$ source ~/.local/bin/virtualenvwrapper.sh"
        print "$ export WORKON_HOME=~/.envs"
        print "$ workon dict-optim"

    logger.info("The following parameters will be used:", extra=de)
    logger.info("- contact email: {} (use only Pasteur email)".format(config.email), extra=de)
    logger.info("- maximum number of jobs: {}".format(config.maxjobs), extra=de)
    logger.info("- first job initiated will have index: {}".format(config.offset), extra=de)
    logger.info("- maximum number of jobs running in parallel: {}".format(config.maxparalleljobs), extra=de)
    logger.info("- running on tars: {}".format(config.useslurm), extra=de)
    logger.info("", extra=de)
    logger.info("Preparing the list of jobs to be submitted", extra=de)

    ## Initialize reconstruction. This writes a to_process.txt file in the results folder.
    njobs = reconstruction.init(config, logger)

    ## Adjust the number of jobs
    if config.maxjobs - config.offset > njobs:
        config.maxjobs = njobs+config.offset-1
    
    ## Now run the reconstruction
    toprocesspath = os.path.join(config.out_folder, 'to_process.txt')
    logpath = os.path.join(config.out_folder, 'reconstruction.log')
    errpath = os.path.join(config.out_folder, 'reconstruction.err')
    cs_scripts = config_general.modules['cs']['bin']
    useslurm = ['false', 'true'][config.useslurm]
    
    if config.useslurm:
        logger.info("Launching SLURM instructions", extra=de)
        subprocess.call(["sbatch",
                         "--mail-type=BEGIN,END",
                         "--mail-user={}".format(config.email),
                         "--array={}-{}%{}".format(config.offset,
                                                   config.maxjobs,
                                                   config.maxparalleljobs),
                         os.path.join(cs_scripts, "reconstruction_init.sh"),
                         toprocesspath],
                        stdout=logpath)
    else:
        logger.info("Launching non-SLURM instructions", extra=de)
        ps = subprocess.Popen(["seq", str(config.maxjobs)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        with open(errpath, 'w') as f:
            with open(logpath, 'w') as g:
                subprocess.call(['parallel',
                                         '-j',
                                         str(config.maxparalleljobs),
                                         os.path.join(cs_scripts, 'reconstruction_init.sh'),
                                         toprocesspath,        #1
                                         cs_scripts,           #2    
                                         logpath,              #3
                                         useslurm,             #4
                                         "{}"],
                                        stdin=ps.stdout, stderr=f, stdout=g)
                ps.wait()
